from django.shortcuts import render

# Create your views here.


import requests

from bs4 import BeautifulSoup
import collections
import re
import json
import csv



def scrape():

    session = requests.Session()
    session.headers = {"User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36"}
    url=  'https://teonite.com/blog/'

    # content = session.get(url, verify = False).content

    req = requests.get(url).text
    soup = BeautifulSoup(req, "html.parser")
    tags = soup.select(".post-title > a[href]")

    for tag in tags:
        saveFile = open('example.txt', 'a')
        # (url + tag.get('href'))
        res = requests.get(url + tag.get('href')).text
        soup2 = BeautifulSoup(res, "html.parser")
        paragraphs = soup2.select(".post-content > p")        
        
        for x in paragraphs:
            article = (x.get_text() + '\n')
            saveFile.write("%s\n" % article)

    # Most common words
    words = re.findall(r"\w+", open('example.txt').read().lower())
    most_common = collections.Counter(words).most_common(10)
    print(most_common)

    saveFile.close()

    setUrl = 'http://localhost:8000/api/starts'
    r =requests.post(setUrl, most_common)
    r.text

    
scrape()